=======================
What is invesy toolbox?
=======================

A set of useful tools, created for my own convenience, but you might find it useful too.

Why "invesy"? Invesy (from German **In**\ halts\ **ve**\ rwaltungs\ **sy**\ stem == content management system) is a closed source cms I created with Thomas Macher. It's only used in-house, that's why we didn't bother making it open source.

Invesy runs on Zope, so most of the individual website's logic runs in restricted Python. That's one reason for this toolbox: providing a set of useful functions in one single package which can be allowed in our restricted Python environment without having to allow a long list of external packages.

That's also why all date and time functions also take into account the old DateTime (as opposed to datetime) package, on which Zope is still relying upon heavily.
