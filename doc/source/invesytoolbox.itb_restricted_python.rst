restricted Python
=================

.. automodule:: invesytoolbox.itb_restricted_python
   :members:
   :undoc-members:
   :show-inheritance:
